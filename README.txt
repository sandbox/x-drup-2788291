eForm Modal module
------------------

This module allows to use eForm forms in modal windows.

Usage
------

Add a block with modal link to a block region.
 - Go to "Admin -> Structure-> Block layout"
 - Click 'Place block' of the region you want to place a eform modal block in.
 - Search for 'Eform Modal Block' in the listed blocks and click 'Place block'.
 - Enter a link title
 - Select the eForm type you want to show in this block.
 - Save the block.
 - Optionally, create another eform modal block.