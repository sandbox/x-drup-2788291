<?php

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\eform\Controller\EFormSubmissionController;
use Drupal\eform\Entity\EFormType;
use Drupal\eform\Entity\EFormSubmission;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * Generate a link for a modal window.
 */
function eform_modal_link($link_title = 'Modal link', $eform_type = '', $form_width = NULL, $form_height = NULL, $ajax_submit = TRUE) {
  $attributes = [
    'class' => ['use-ajax'],
    'data-dialog-type' => 'modal'
  ];

  $dialog_options = [];
  if (is_numeric($form_width)) $dialog_options['width'] = $form_width;
  if (is_numeric($form_height)) $dialog_options['height'] = $form_height;
  if (!empty($dialog_options)) $attributes['data-dialog-options'] = Json::encode($dialog_options);

  $url = new Url('entity.eform_submission.submit_page', ['eform_type' => $eform_type]);
  $url->setOption('attributes', $attributes);
  $url->setOption('query', [
    'ajax_submit' => $ajax_submit
  ]);

  return \Drupal::l($link_title, $url);
}

/**
 * Implements hook_preprocess_html().
 */
function eform_modal_preprocess_html(&$variables) {
  $variables['page']['#attached']['library'][] = 'core/drupal.ajax';
}

/**
 * Implements hook_form_alter().
 */
function eform_modal_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if (Unicode::substr($form_id, 0, 17) == 'eform_submission_' && Unicode::substr($form_id, -12) == '_submit_form') {
    $ajax_submit = \Drupal::request()->query->get('ajax_submit');
    if ($ajax_submit == true) {
      $form['actions']['submit']['#ajax'] = [
        'callback' => 'eform_modal_ajaxSubmit',
        'wrapper' => 'eform_wrapper'
      ];
      $form['#prefix'] = '<div id="eform_wrapper">';
      $form['#suffix'] = '</div>';
    }
  }
}

function eform_modal_ajaxSubmit(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $message_data = drupal_get_messages(NULL, FALSE);
  // Check to see if there were any errors with the form submission
  if (count($message_data['error']) == 0) {
    $form_state->disableRedirect($no_redirect = false);
    $redirect = $form_state->getRedirect();
    $route_parameters = $redirect->getRouteParameters();

    $submission_controller = new EFormSubmissionController();
    $eform_type = EFormType::load($route_parameters['eform_type']);
    $eform_submission = EFormSubmission::load($route_parameters['eform_submission']);

    $title = $submission_controller->confirmationTitle($eform_type);
    $content = drupal_render($submission_controller->confirmPage($eform_type, $eform_submission));

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($title, $content));
    return $response;
  }
  else return $form;
}