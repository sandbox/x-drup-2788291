<?php

/**
 * @file
 * Contains \Drupal\eform_modal\Plugin\Block\EformModalBlock.
 */

namespace Drupal\eform_modal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides eform modal block.
 *
 * @Block(
 *   id = "eform_modal_block",
 *   admin_label = @Translation("Eform Modal Block"),
 *   category = @Translation("Blocks")
 * )
 */

class EformModalBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();
    $link = eform_modal_link($config['link_title'], $config['eform_type'], $config['form_width'], $config['form_height'], $config['ajax_submit']);
    return ['#markup' => $link];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();

    $eform_types = \Drupal::entityQuery('eform_type')->execute();
    foreach ($eform_types as $id) {
      $eform = entity_load('eform_type', $id);
      $eform_types[$id] = $eform->name;
    }

    // Add a form fields to the existing block configuration form.
    $form['link_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link title'),
      '#default_value' => isset($config['link_title']) ? $config['link_title'] : 'Eform modal',
      '#required' => TRUE,
    ];
    $form['eform_type'] = [
      '#type' => 'select',
      '#empty_value' => '',
      '#options' => $eform_types,
      '#title' => $this->t('Eform type'),
      '#default_value' => isset($config['eform_type']) ? $config['eform_type'] : '',
      '#required' => TRUE,
    ];
    $form['form_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form width'),
      '#default_value' => isset($config['form_width']) ? $config['form_width'] : '',
    ];
    $form['form_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form height'),
      '#default_value' => isset($config['form_height']) ? $config['form_height'] : '',
    ];
    $form['ajax_submit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ajax submit'),
      '#default_value' => isset($config['ajax_submit']) ? $config['ajax_submit'] : true,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Save our custom settings when the form is submitted.
    $keys = array('link_title', 'eform_type', 'form_width', 'form_height', 'ajax_submit');
    foreach ($keys as $key) $this->setConfigurationValue($key, $form_state->getValue($key));
  }

}